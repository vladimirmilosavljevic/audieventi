package com.eng.audieventi

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


object DataGlobal {


    var contactAndInfoObject = ContactsAndInfo()
    var audiLangs:Map<String,String> = mutableMapOf()

    val programExperienceList:ArrayList<ProgramExperience> = arrayListOf()
    val foodExperienceList:ArrayList<FoodExperience> = arrayListOf()
    val allergensList:ArrayList<Allergens> = arrayListOf()
    val calendarEventList :ArrayList<CalendarEvent> = arrayListOf()
    val notificationList:ArrayList<Notifications> = arrayListOf()
    var placesAndTerritoryObject= Places()
    var audiDrivingExperienceObject = AudiDrivingExperience()
    var couponObject =  Coupon()
    var programObject = Program()

    val clientId = "abc123"
    val clientSecret = "abcd123"


    //json constants
    val jsonRoot = "http://mobileapp-coll.engds.it/AudiEventi/"
    val jsonLangs ="langs/it_IT.json"
    val jsonConfig = "config/config.json"




    data class CalendarEvent(
        val title: String,
        val description: String,
        val image: String
    )



    data class FoodExperience(
        val id: String,
        val title: String,
        val header: String,
        val subtitleFood: String,
        val imageBackground: String,
        val programExperience: ArrayList<ProgramExperience> = arrayListOf()
    )

    data class ProgramExperience(
        val day: String,
        val start: String,
        val type: String,
        val activity: String,
        val site: String,
        val description: String,
        val food: String,
        val allergens: ArrayList<Allergens> = arrayListOf()
    )


    data class Places(

        val id: String ="",
        val title: String="",
        val subtitle_places: String="",
        val title_places: String="",
        val description: String="",
        val carousel_places: ArrayList<String> = arrayListOf(),
        val image_places: String=""
    )

    data class AudiDrivingExperience(
        val id: String="",
        val description: String="",
        val subtitle_ade: String="",
        val main_title: String="",
        val title_ade: String="",
        val carousel_ade: ArrayList<String> = arrayListOf(),
        val image_ade: String=""
    )



    data class Coupon(
        val action: String="",
        val couponProp: String="",
        val eventId: String="",
        val status: String="",
        val eventStatus: String="",
        val valId: String="",
        val result: String="",
        val resultCode: String="",
        val resultMessage:String=""
    )

    data class ContactsAndInfo(
        val id: String="",
        val free_text: String="",
        val imageBackground: String="",
        val contactList: ArrayList<Contact> = arrayListOf()

    )

    data class Contact(
        val name: String,
        val place: String,
        val street: String,
        val region: String,
        val phone: String,
        val phone_fix: String,
        val email: String
    )

    data class Notifications(
        val date: String,
        val title: String,
        val time: String,
        val description: String

    )

    data class Activity(
        val start: String,
        val end: String,
        val activityName: String,

        )

    data class DailyActivity(
        val day: String,
        val activityList: ArrayList<Activity> = arrayListOf()
    )


    data class Program(
        val image: String="",
        val title: String="",
        val subtitle:String = "",
        val note:String="",
        val description: String="",
        val infoUri:String="",
        val dailyActivityList: ArrayList<DailyActivity> = arrayListOf()
    )

    data class Allergens(

        val name:String
    )

}
