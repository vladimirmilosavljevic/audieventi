package com.eng.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

@Suppress("DEPRECATION")
class SplashActivity : AppCompatActivity() {

    val jsonData = JsonData()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        jsonData.getJson().execute(jsonData.fullPath(DataGlobal.jsonRoot, DataGlobal.jsonLangs))
        TestData().populateTestData()



        check()

    }

    fun check() {
        Handler(Looper.getMainLooper()).postDelayed({

            if (DataGlobal.audiLangs.count() != 72) {
                check()
            } else {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }

        }, 1000)
    }
}