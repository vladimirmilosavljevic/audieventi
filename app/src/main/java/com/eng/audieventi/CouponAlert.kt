package com.eng.audieventi

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.layout_coupon.view.*

class CouponAlert {

    fun showAlert(context: Context) {

        val mDialogView = LayoutInflater.from(context).inflate(R.layout.layout_coupon, null)

        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)

        mDialogView.couponloginmessage.text = DataGlobal.audiLangs.get("COUPON_LOGIN_MESSAGE")
        mDialogView.confermaButton.text = DataGlobal.audiLangs.get("COUPON_LOGIN_BUTTON")


        //show dialog
        val mAlertDialog = mBuilder.show()
        //login button click of dialog
        mDialogView.confermaButton.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            //get text from EditText of dialog
            val coupon = mDialogView.etcouponValue.text.toString()
        }
        //cancel button click of dialog
        mDialogView.ivclose.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
        }
    }

}
