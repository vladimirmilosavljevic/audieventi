package com.eng.audieventi

import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

val URL = "www.audi.it"

class MainActivity : AppCompatActivity() {
    val couponAlert = CouponAlert()

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
          supportActionBar?.hide()
        
        langsData()
        alertDialogOpen()
        setNavView()
        redirectingToAudiWebsite()


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun langsData() {

        val tvEventiAudi = findViewById<TextView>(R.id.tveventiaudi)
        tvEventiAudi.text = DataGlobal.audiLangs.get("EVENT_LIST_HEADER")

        val tvscoprilenostre = findViewById<TextView>(R.id.tvscoprilenostre)
        tvscoprilenostre.text = DataGlobal.audiLangs.get("HOME_GUEST_EVENT_LIST_BUTTON")

        val tviltuo = findViewById<TextView>(R.id.tviltuo)
        tviltuo.text = DataGlobal.audiLangs.get("HOME_GUEST_COUPON_LOGIN")

        val tvinseriscicoupon = findViewById<TextView>(R.id.tvinseriscicoupon)
        tvinseriscicoupon.text = DataGlobal.audiLangs.get("HOME_GUEST_COUPON_LOGIN_BUTTON")

        val gammaaudi = findViewById<TextView>(R.id.gammaaudi)
        gammaaudi.text = DataGlobal.audiLangs.get("HOME_GUEST_AUDI_RANGE")

        val tvscopriimodeli = findViewById<TextView>(R.id.tvscopriimodeli)
        tvscopriimodeli.text = DataGlobal.audiLangs.get("HOME_GUEST_AUDI_RANGE_BUTTON")


    }

    fun closeDrawlerAndOpenCouponDialog() {
        drawerLayout.closeDrawer(nav_view)
        CouponAlert().showAlert(this)
    }

    fun setNavView() {
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        hamburgerImage.setOnClickListener {

            val menu = nav_view.menu
            val programma = menu.findItem(R.id.programma)
            val foodExpirience = menu.findItem(R.id.foodExpirience)
            val luoghieETerritorio = menu.findItem(R.id.luoghiETerritorio)
            val audiDrivingExperience = menu.findItem(R.id.adeMenuItem)
            val sondaggio = menu.findItem(R.id.sondaggio)
            val infoContatti = menu.findItem(R.id.infoContatti)
            programma.setVisible(true)
            foodExpirience.setVisible(true)
            luoghieETerritorio.setVisible(true)
            audiDrivingExperience.setVisible(true)
            sondaggio.setVisible(true)
            infoContatti.setVisible(true)
            drawerLayout.openDrawer(nav_view)
        }

        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.homeEventi -> Toast.makeText(this, "Message item", Toast.LENGTH_SHORT).show()
                R.id.programma -> null
                R.id.foodExpirience -> null
                R.id.luoghiETerritorio -> null
                R.id.adeMenuItem -> null
                R.id.sondaggio -> null
                R.id.calendarioEventi -> startActivity(Intent(this, CalendarEvent::class.java))
                R.id.couponeventi -> closeDrawlerAndOpenCouponDialog()
                R.id.infoContatti -> null

            }
            true
        }
    }

    fun alertDialogOpen() {


        layoutIlTuoEvento.setOnClickListener {
            couponAlert.showAlert(this)
        }
    }


    fun redirectingToAudiWebsite() {
        val chrome = findViewById<RelativeLayout>(R.id.gammaAudiLayout)

        chrome.setOnClickListener {
            val uri = Uri.parse("googlechrome://navigate?url=" + URL)
            val intent = Intent(Intent.ACTION_VIEW, uri)

            startActivity(intent)
        }
    }


}
