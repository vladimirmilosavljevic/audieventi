package com.eng.audieventi

import android.os.AsyncTask
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class JsonData {


    inner class getJson : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val conn = URL(params[0]).openConnection() as HttpURLConnection

            try {
                conn.connect()
                json = conn.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                conn.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            println(result)
            readLangJson(result)
            println(DataGlobal.audiLangs.size.toString())
            for (keyVal in DataGlobal.audiLangs) {
                println(keyVal)
            }
         //   printData(result)

        }
    }


    fun fullPath(root: String, extension: String): String {

        return root + extension
    }

    fun readLangJson(json: String?) {
        val jsonObj = JSONObject(json)
        DataGlobal.audiLangs = jsonObj.toMap() as Map<String, String>
    }

    fun JSONObject.toMap(): Map<String, Any?> = keys().asSequence().associateWith {
        when (val value = this[it]) {
            is JSONArray -> {
                val map = (0 until value.length()).associate { Pair(it.toString(), value[it]) }
                JSONObject(map).toMap().values.toList()
            }
            is JSONObject -> value.toMap()
            JSONObject.NULL -> null
            else -> value
        }
    }
}